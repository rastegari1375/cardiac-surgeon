import csv

from flask import Flask, jsonify, request, send_file
from flask_cors import CORS

from db import forms

app = Flask(__name__)
CORS(app)
app.debug = True


@app.route('/submit-form', methods=['POST'])
def submit_form():
    try:
        form = request.get_json()
    except Exception as ex:
        print(ex)
        return jsonify(status='ERROR', message='مشکل در ارسال فایل رخ داده است'), 400

    # TODO: validation data

    forms.insert_one(form)

    return jsonify(status='OK', message='فرم شما ارسال شد'), 200


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


@app.route('/download-db', methods=['GET'])
def download_db():
    db = list(forms.find())
    for item in db:
        del item['_id']

    with open('forms.csv', 'w') as f:
        fieldnames = ["ID",
                      "phoneNumber",
                      "patlentID",
                      "name",
                      "education",
                      "countryCode",
                      "dateOperation",
                      "nchilds",
                      "age",
                      "marriageStatus",
                      "gender",
                      "anginaCCS",
                      "dyspnoea",
                      "numbarOfPreviousMyocardial",
                      "mostRecentMyocardialInfarction",
                      "congestiveHeartFailure",
                      "previousPCI",
                      "previousCardiacVascularOfThoracicSurgery",
                      "smokingHistory",
                      "weight",
                      "height",
                      "drugHistory",
                      "job",
                      "hospitalCode",
                      "dateOfBirth",
                      "city",
                      "dateOfAdmission",
                      "dateOfDischarge",
                      "dateOfLastPCI",
                      "dateOfLastCardiacSurgery",
                      "drugHX",
                      "drugHXText",
                      "equivalentDosOfSimvastatin",
                      "addiction",
                      "lastLabTest",
                      "DiabetesTreatment",
                      "hypertension",
                      "ypercholesterolaemia",
                      "historyOfKidneyDX",
                      "renal",
                      "leftVentricularFunctionEjectionFraction",
                      "LastPreOperativeCreatinine",
                      "lastPreOperaiveLabTests",
                      "choromicLungDisease",
                      "extraCardiacArteriopathy",
                      "CerebroVascuarDiseaseType",
                      "NeurolgocialDysfunction",
                      "CarotidBruits",
                      "preOperativeHeartRhythm",
                      "leftRrRightHearthCatheterization",
                      "dateOfLastCatheterization",
                      "numberOfDiseasedCoronaryVessels",
                      "leftMainStemDisease",
                      "ejectionFractionCategory",
                      "rjectionFractionValue",
                      "psaSystolic",
                      "levedp",
                      "avGradent",
                      "diseasedVessle",
                      "meanPawpLa",
                      "ivNitratesHeparinOfAnyKind",
                      "ivIntropes",
                      "ventilated",
                      "cardiogenicShok",
                      "proOperativeHeartRhythm",
                      "postOperation",
                      "postOperationLabTest",
                      "postOperationLabTestAfter24",
                      "postOperationLabTestAfter48",
                      "postOperationTreatment",
                      "durationOfIntubation",
                      "cardiopulmonary",
                      "aroticCrossClampingDuration",
                      "labp",
                      "labpCheckBox",
                      "familyHx",
                      "familyHxOfAf",
                      "exerciseToleranceTestETT",
                      "operativeUrgency",
                      "intraArorticPallonPump",
                      "cerebroVascuarDiseaseType",
                      "numberOfPreviousHearthOperations",
                      "procedureGroups",
                      "procedureGroupsNew",
                      "typeOfOperation",
                      "otherCardiac",
                      "otherNonCardiac",
                      "distalCoronaryAnastamosesArtrialConduits",
                      "distalCoronaryAnastamosesVenousConduits",
                      "vessleWhichHaveBeenGrafted",
                      "arteryIesUsedAsGrafts",
                      "otherCardiacProceduresDetails",
                      "endarterectomizeVessle",
                      "otherNonCardiacProceduresDetails",
                      "segmentsOfTheAorta",
                      "aorticProcedure",
                      "poorMobility",
                      "criticalPreoperativeState",
                      "surgeryOnThoracicAorta",
                      "pulmonaryHypertension",
                      "stenosis",
                      "sufficiency",
                      "explantType",
                      "nativeValvePathology",
                      "ReasonsForRepeatValveSurgery",
                      "valveProcedure",
                      "implantType",
                      "implantCode",
                      "implantSize",
                      "durationOfIcuStay",
                      "durationOfHospitalStay",
                      "activeEndocarditis"]
        cw = csv.DictWriter(f, fieldnames=fieldnames)
        cw.writeheader()
        cw.writerows(db)

    return send_file('forms.csv', mimetype='text/csv', as_attachment=True, attachment_filename='forms.csv', cache_timeout=0)


if __name__ == "__main__":
    app.run(port='5678', host='127.0.0.1')
